var fs = require('fs');

module.exports = {
  position: function () {
    var posArray = fs.readFileSync('./data/Position.txt').toString().split("\r\n");
    for(i in posArray) {
      posArray[i] = posArray[i].split(/,| /);
    }
    return posArray;
  },
  sleepProfile: function () {
      var sleepArray = fs.readFileSync('./data/Sleep_profile.txt').toString().split("\r\n");
      for(i in sleepArray) {
        sleepArray[i] = sleepArray[i].split(/,| /);
      }
      return sleepArray;
  },
  spindles: function () {
      var spindleArray = fs.readFileSync('./data/Spindle_K.txt').toString().split("\r\n");
      for(i in spindleArray) {
        spindleArray[i] = spindleArray[i].split(/,| |;/);
      }
      return spindleArray;
  }
};
