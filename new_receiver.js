'use strict';

var readFiles = require('./lib/readTextFiles');
var osc = require('./node_modules/node-osc/lib');
var NanoTimer = require('nanotimer');
var moment = require('./node_modules/moment');
var argv = require('minimist')(process.argv.slice(2));

var lineTimer = new NanoTimer();

var msg =  new osc.Message('/address');
var client = new osc.Client('192.168.1.2', 3333);
var client_arduino = new osc.Client('192.168.1.2', 3334);

var serialport = require('serialport');
var SerialPort = serialport.SerialPort;
var parsers = serialport.parsers;
var portOpened = false;
var sendSerialValues = false;
var serial_write;
var k=0;
var y = 0;
var j = 0;
var counter = 0;
var EKG = 0.0;
var CHIN = 0.0;
var HEART_RATE = 0.0;
var THORAX = 0.0;
var THORAX_S = 0;
var PREV_THORAX = 0.0;
var EOGL = 0.0;
var EOGL_S = 0;
var PREV_EOGL = 0.0;
var EOGR = 0.0;
var EOGR_S = 0;
var PREV_EOGR = 0.0;



var values = [];
var myarray = [];
var serial_every = 10;
var timeInterval = 2;

var startSleepTime = moment(new Date('2016-04-22 22:20:01.000'));
var startScriptTime = moment();
var valuesDateTimeRunning = moment(new Date('2016-04-22 22:20:01.000'));; //actual datetime from the raw values
var valuesTime; //milliseconds from raw values
var realMsRunning; // real running time in millisecons
var timeOffset; //time offset beetween real and values time running

var position;
var pos_counter =0;
var spindles;
var spindle_counter =0;
var sleepProfile;
var sleepProf_counter =0;

var position_value="";
var pos_num = 0;
var sleep_profile_value="";
var profile_num = 0;
var spindle_value="";

var gotoTime = false; // insert time in minutes
var start_byte = 0;
var offsetStartTime = 0;

var heart_value = 0;
var profile_value = 0;
var spindle_value = 0;

var last_heart_value = 0;
var last_profile_value = 0;
var last_spindle_value = 0;

if(argv.t){
  gotoTime = argv.t;
  start_byte = gotoTime*2121000;
}


for(var i=0; i<=32;i++){
  values[i] = 0.0;
}

var prev = 0;

if(!argv.s) {
var port = new SerialPort('/dev/cu.usbmodem411', {
  baudrate: 115200,
  parser: parsers.readline('\r\n')
});
port.on('open', function() {
  console.log('Port open');
  setTimeout(()=>{
    portOpened = true;
  }, 1000);
});
}


var oscServer = new osc.Server(3334, '0.0.0.0');
oscServer.on("message", function (msg, rinfo) {

      sendSerial(msg);
});


function sendSerial(values){
  counter++;
  if(counter == serial_every){

    heart_value = parseFloat(values[1]).toFixed(0);
    profile_value = transform_profile(values[2]);
    spindle_value = values[3].split(' ')[0];

    serial_write = heart_value+" "+profile_value+" "+spindle_value+"/";

    counter = 0;

    if(last_heart_value != heart_value) {
      last_heart_value = heart_value;
      sendSerialValues = true;
    }
    if( last_profile_value != profile_value ) {
      last_profile_value = profile_value;
      sendSerialValues = true;
    }
    if(last_spindle_value != spindle_value ) {
      last_spindle_value = spindle_value;
      sendSerialValues = true;
    }

    if(portOpened){
      if(sendSerialValues){
        console.log(serial_write);
        sendSerialBuffer(serial_write);
      }
    }

  }
}
function transform_profile(t){
  if(t == "Wake"){
  profile_num = 1;
  }
  else if(t == "Rem"){
    profile_num = 2;
  }
  else if(t == "N1"){
    profile_num = 3;
  }
  else if(t == "N2"){
    profile_num = 4;
  }
  else if(t == "N3"){
    profile_num = 5;
  }
  else if(t == "N4"){
    profile_num = 6;
  }
  return profile_num;
}
function transform_position(t){
  if(t == "A"){
  pos_num = 1;
  }
  else if(t == "Prone"){
    pos_num = 2;
  }
  else if(t == "Supine"){
    pos_num = 3;
  }
  else if(t == "Upright"){
    pos_num = 4;
  }
  else if(t == "Left"){
    pos_num = 5;
  }
  else if(t == "Right"){
    pos_num = 6;
  }
  return pos_num;
}


function sendSerialBuffer(data){
  for(var i=0; i<data.length; i++){
    port.write(new Buffer(data[i], 'ascii'), function(err, results) {
      if (err) {
        console.log('Error on serialport write: ', err.message);
        port.open();
      }
    });
  }
  sendSerialValues = false;
}
