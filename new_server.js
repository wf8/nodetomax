'use strict';

var readFiles = require('./lib/readTextFiles');
var osc = require('./node_modules/node-osc/lib');
var NanoTimer = require('nanotimer');
var moment = require('./node_modules/moment');
var argv = require('minimist')(process.argv.slice(2));

var lineTimer = new NanoTimer();

var msg =  new osc.Message('/address');
var client = new osc.Client('192.168.1.11', 3333);
var client_arduino = new osc.Client('192.168.1.11', 3334);

var serialport = require('serialport');
var SerialPort = serialport.SerialPort;
var parsers = serialport.parsers;
var portOpened = false;

var k=0;
var y = 0;
var j = 0;
var counter = 0;
var EKG = 0.0;
var CHIN = 0.0;
var HEART_RATE = 0.0;
var THORAX = 0.0;
var THORAX_S = 0;
var PREV_THORAX = 0.0;
var EOGL = 0.0;
var EOGL_S = 0;
var PREV_EOGL = 0.0;
var EOGR = 0.0;
var EOGR_S = 0;
var PREV_EOGR = 0.0;



var values = [];
var myarray = [];
var serial_every = 10;
var timeInterval = 2;

var startSleepTime = moment(new Date('2016-04-22 22:20:01.000'));
var startScriptTime = moment();
var valuesDateTimeRunning = moment(new Date('2016-04-22 22:20:01.000'));; //actual datetime from the raw values
var valuesTime; //milliseconds from raw values
var realMsRunning; // real running time in millisecons
var timeOffset; //time offset beetween real and values time running

var position;
var pos_counter =0;
var spindles;
var spindle_counter =0;
var sleepProfile;
var sleepProf_counter =0;

var position_value="";
var pos_num = 0;
var sleep_profile_value="";
var profile_num = 0;
var spindle_value="";

var gotoTime = false; // insert time in minutes
var start_byte = 0;
var offsetStartTime = 0;


if(argv.t){
  gotoTime = argv.t;
  start_byte = gotoTime*2121000;
}

// ======================
// READ FILES
// ======================
position = readFiles.position();
// console.log(position[0][0]);
// console.log(position[1][2]);
sleepProfile = readFiles.sleepProfile();
// console.log(sleepProfile[0][0]);
// console.log(sleepProfile[0][2]);
spindles = readFiles.spindles();
// console.log(spindles[0][0]);
// console.log(spindles[0][5]);


for(var i=0; i<=32;i++){
  values[i] = 0.0;
}

var start = new Date();
var prev = 0;

if(!argv.s) {
var port = new SerialPort('/dev/cu.usbmodem1411', {
  baudrate: 250000,
  parser: parsers.readline('\r\n')
});
port.on('open', function() {
  console.log('Port open');
  setTimeout(()=>{
    portOpened = true;
  }, 1000);

});
}

readFile();

function readFile(){
  var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('data/raw_data.txt',{
      start: start_byte,
      highWaterMark : 10
    })
  });

  lineReader.on('line', function (line) {
    // var array = line.split('*');
      setTimeout(()=>{
          lineReader.resume();
      }, timeInterval);
      // lineTimer.setTimeout(pause, '', '4275u');
      // function pause(){
      //   lineReader.resume();
      // }
      // console.log(line);
      slitArray(line);
      lineReader.pause();
    // sendData(parseFloat(array[0]));
  });
  lineReader.on('close', function() {
    console.log('Finished reading file');
    console.log(start);
    console.log(new Date());
    finished = true;
  });
}




function sendSerial(){
  // console.log(myarray[0]);
  // console.log((now-start)/1000);
  HEART_RATE += parseFloat(values[14]);
  THORAX += parseFloat(values[11]);
  EOGL += parseFloat(values[1]);
  EOGR += parseFloat(values[2]);
  counter++;
  if(counter == serial_every){
    var f_THORAX = filterSlideT(THORAX,1000);
    var f_EOGL = filterSlideEL(EOGL,1000);
    var f_EOGR = filterSlideER(EOGR,1000);
    // var serial_write = EKG/serial_every+" "+CHIN/serial_every+" "+HEART_RATE/serial_every+" "+f_THORAX/serial_every+"/";
    THORAX_S = f_THORAX/serial_every;
    EOGL_S = f_EOGR/serial_every;
    EOGR_S = f_EOGL/serial_every;

    var serial_write = values[0]+" "+HEART_RATE/serial_every+" "+THORAX_S.toFixed(1)+" "+EOGR_S.toFixed(1)+" "+EOGR_S.toFixed(1)+" "+transform_position(position_value)+" "+transform_profile(sleep_profile_value)+" "+spindle_counter+"/";

    // var serial_write =values[0]+" "+HEART_RATE/serial_every+" "+f_THORAX/serial_every+" "+f_EOGL/serial_every+" "+f_EOGR/serial_every+" "+position_value+" "+sleep_profile_value+" "+spindle_value+"/";
    var time_s = parseFloat(values[0]).toFixed(0);
    // var serial_write =parseInt(time_s)+" "+HEART_RATE/serial_every+" "+f_THORAX/serial_every+" "+f_EOGL/serial_every+" "+f_EOGR/serial_every+" "+transform_position(position_value)+" "+transform_profile(sleep_profile_value)+" "+spindle_counter+"/";
    HEART_RATE =0;
    THORAX =0;
    EOGL = 0;
    EOGR = 0;
    // var serial_write = "HELLO";
    // var buff = Buffer.from('test');
    counter = 0;
    if(portOpened){
      sendSerialBuffer(serial_write);
    }
    // port.write(new Buffer('0 0 80 0/','ascii'));
  }
}
function transform_profile(t){
  if(t == "Wake"){
  profile_num = 1;
  }
  else if(t == "Rem"){
    profile_num = 2;
  }
  else if(t == "N1"){
    profile_num = 3;
  }
  else if(t == "N2"){
    profile_num = 4;
  }
  else if(t == "N3"){
    profile_num = 5;
  }
  else if(t == "N4"){
    profile_num = 6;
  }
  return profile_num;
}
function transform_position(t){
  if(t == "A"){
  pos_num = 1;
  }
  else if(t == "Prone"){
    pos_num = 2;
  }
  else if(t == "Supine"){
    pos_num = 3;
  }
  else if(t == "Upright"){
    pos_num = 4;
  }
  else if(t == "Left"){
    pos_num = 5;
  }
  else if(t == "Right"){
    pos_num = 6;
  }
  return pos_num;
}
function filterSlideT(rawValue,slide) {
  // run the filter:
  var result = PREV_THORAX +((rawValue - PREV_THORAX)/slide);
  PREV_THORAX = THORAX;
  // return the result:
  return result;
}
function filterSlideEL(rawValue,slide) {
  // run the filter:
  var result = PREV_EOGL +((rawValue - PREV_EOGL)/slide);
  PREV_EOGL = EOGL;
  // return the result:
  return result;
}
function filterSlideER(rawValue,slide) {
  // run the filter:
  var result = PREV_EOGR +((rawValue - PREV_EOGR)/slide);
  PREV_EOGR = EOGR;
  // return the result:
  return result;
}
function sendSerialBuffer(data){
  for(var i=0; i<data.length; i++){
    port.write(new Buffer(data[i], 'ascii'), function(err, results) {
    });
  }
}

function convertToAscii(str) {
  var result = '';
  var currentChar = '';
  var i = 0;
  for (; i < str.length; i += 1) {
    currentChar = str[i].charCodeAt(0).toString(2);
    if (currentChar.length < 8) {
      while (8 - currentChar.length) {
        currentChar = '0' + currentChar;
      }
    }
    result += currentChar;
  }
  return result;
}
if(!argv.s) {
  port.on('data', function(data) {
    console.log(data);
  });
}


function sendUDP(){
    msg =  new osc.Message('/s',values[0],values[1],values[2],values[3],values[4],values[5],values[6],values[7],values[8],values[9],values[10],
                                values[11],values[12],values[13],values[14],values[15],values[16],values[17],values[18],values[19],values[20],
                                values[21],values[22],values[23],values[24],values[25],position_value,sleep_profile_value,spindle_value);
    client.send(msg);
    client_arduino.send(msg);

}


function slitArray(a){
  var string = a.toString();
  myarray = string.split(',');

  for(var i = 0; i < myarray.length; i++)
  {
    if(!myarray[i]){
      myarray[i] = "x";
    }
    if(parseFloat(myarray[i]) > parseFloat(values[i]) || parseFloat(myarray[i]) < parseFloat(values[i]) ){
      values[i] = parseFloat(myarray[i]);
    }
  }
  sendUDP();
  if(!argv.s){
    sendSerial();
  }

    timeDiff();

}

function timeDiff(){
  if(y == 0) { startScriptTime = moment(); y=1;}
  realMsRunning = moment().diff(startScriptTime) + offsetStartTime*1000;
  valuesTime =  moment.duration(values[0], 'seconds');
  timeOffset = realMsRunning - valuesTime;
  // var offset = (msRunning-values[0]*1000);
  // console.log("real: "+realMsRunning);
  // console.log("values Time: "+valuesTime);

  if(timeOffset > 0) {
    timeInterval = 1;
  }
  else if(timeOffset<0){
    timeInterval = 4;
  }
  else {
    timeInterval = 2;
  }
  j++;
  if(j==10){
    offsetStartTime = values[0];
    j=10;
    for(var i=0;i<1000;i++){
      valuesDateTimeRunning = moment(new Date('2016-04-22 22:20:01.000'));
      valuesDateTimeRunning.add(values[0],'s');
      checkTexts();
    }
  }
  if(argv.d){
    if(values[0] % 5 == 0) {
      console.log("****************************");
      console.log("Diff: "+ timeOffset+" ms");
      console.log("Real time running: "+realMsRunning+" ms | " + moment().diff(startScriptTime,'minutes')+" min");
      console.log("Values Time: "+valuesTime+" sec");
      console.log("Raw value Time: "+values[0]+" sec");
      console.log(valuesDateTimeRunning.format("YYYY-MM-DD HH:mm:ss.SSS"));
      console.log("****************************");
    }
  }
  if(values[0] % 1 == 0) {
    valuesDateTimeRunning = moment(new Date('2016-04-22 22:20:01.000'));
    valuesDateTimeRunning.add(values[0],'s');
    // calculateCurrentTime(new Date('2016-04-22 '+msToTime(timeRunning)));
    checkTexts();
  }

}

function msToTime(s) {
  function addZ(n) {
    return (n<10? '0':'') + n;
  }
  var ms = s % 1000;
  s = (s - ms) / 1000;
  var secs = s % 60;
  s = (s - secs) / 60;
  var mins = s % 60;
  var hrs = (s - mins) / 60;

  return addZ(hrs) + ':' + addZ(mins) + ':' + addZ(secs) + '.' + ms;
}

function calculateCurrentTime(timeRunning){


}

function checkTexts(){
  //Check Position
  if(pos_counter <= position.length) {
    //get hour to see if after midnight
    var hour = position[pos_counter][0].split(':')[0];
    var day = '22';
    if(parseInt(hour) != 22 && parseInt(hour) != 23){
      day = '23';
    }
    var date = moment(new Date('2016-04-'+day+' '+position[pos_counter][0]));
    if(argv.d){
      console.log('---------------------------');
      console.log("Position event in: "+(date.diff(valuesDateTimeRunning)/1000)+" sec");
    }
    if(date.diff(valuesDateTimeRunning)<0) {
      position_value = position[pos_counter][2];
      pos_counter++;
      if(argv.d){
        console.log(position_value);
      }
    }
  }
  //
  // //Check Sleep Profile
  if(sleepProf_counter <= sleepProfile.length) {
    //get hour to see if after midnight
    var hour = sleepProfile[sleepProf_counter][0].split(':')[0];
    var day = '22';
    if(parseInt(hour) != 22 && parseInt(hour) != 23){
      day = '23';
    }
    var date = moment(new Date('2016-04-'+day+' '+sleepProfile[sleepProf_counter][0]));
    if(argv.d){
      console.log("Sleep event in: "+(date.diff(valuesDateTimeRunning)/1000)+" sec");
    }
    if(date.diff(valuesDateTimeRunning)<0) {
      sleep_profile_value = sleepProfile[sleepProf_counter][2];
      sleepProf_counter++;
      if(argv.d){
        console.log(sleep_profile_value);
      }
    }
  }
  // //Check Spindles
  if(spindle_counter <= spindles.length) {
    //get hour to see if after midnight
    var hour = spindles[spindle_counter][0].split(':')[0];
    var day = '22';
    if(parseInt(hour) != 22 && parseInt(hour) != 23){
      day = '23';
    }
    var date = moment(new Date('2016-04-'+day+' '+spindles[spindle_counter][0]));
    if(argv.d){
      console.log("Spindle event in: "+(date.diff(valuesDateTimeRunning)/1000)+" sec");
    }
    if(date.diff(valuesDateTimeRunning)<0) {
      spindle_value = spindle_counter+' '+spindles[spindle_counter][5]+' '+spindles[spindle_counter][4];
      spindle_counter++;
      if(argv.d){
        console.log(spindle_value);
      }
    }
  }
  if(argv.d){
    console.log('---------------------------');
  }

}
