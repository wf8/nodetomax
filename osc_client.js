'use strict';

var readFiles = require('./lib/readTextFiles');
var osc = require('./node_modules/node-osc/lib');
var NanoTimer = require('nanotimer');
var argv = require('minimist')(process.argv.slice(2));
console.dir(argv.s);
var lineTimer = new NanoTimer();

var msg =  new osc.Message('/address');
var client = new osc.Client('127.0.0.1', 3333);


var serialport = require('serialport');
var SerialPort = serialport.SerialPort;
var parsers = serialport.parsers;
var portOpened = false;

var k=0;
var y = 0;
var counter = 0;
var EKG = 0.0;
var CHIN = 0.0;
var HEART_RATE = 0.0;
var THORAX = 0.0;
var PREV_THORAX = 0.0;
var values = [];
var myarray = [];
var serial_every = 10;
var timeInterval = 2;
var startSleepTime = new Date('2016-04-22 22:20:01.300');
var currentSleepTime = new Date('2016-04-22 22:20:01:567');
var position;
var pos_counter =0;
var spindles;
var spindle_counter =0;
var sleepProfile;
var sleepProf_counter =0;

var position_value="";
var sleep_profile_value="";
var spindle_value="";

// ======================
// READ FILES
// ======================
position = readFiles.position();
// console.log(position[0][0]);
// console.log(position[1][2]);
sleepProfile = readFiles.sleepProfile();
// console.log(sleepProfile[0][0]);
// console.log(sleepProfile[0][2]);
spindles = readFiles.spindles();
// console.log(spindles[0][0]);
// console.log(spindles[0][5]);


for(var i=0; i<=32;i++){
  values[i] = 0.0;
}

var start = new Date();
var prev = 0;

if(!argv.s) {
var port = new SerialPort('/dev/cu.usbmodem14711', {
  baudrate: 250000,
  parser: parsers.readline('\r\n')
});
port.on('open', function() {
  console.log('Port open');
  setTimeout(()=>{
    portOpened = true;
  }, 1000);

});
}

readFile();

function readFile(){
  var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('data/raw_data.txt',{
      highWaterMark : 10
    })
  });

  lineReader.on('line', function (line) {
    // var array = line.split('*');
      setTimeout(()=>{
          lineReader.resume();
      }, timeInterval);
      // lineTimer.setTimeout(pause, '', '4275u');
      // function pause(){
      //   lineReader.resume();
      // }
      // console.log(line);
      slitArray(line);
      lineReader.pause();
    // sendData(parseFloat(array[0]));
  });
  lineReader.on('close', function() {
    console.log('Finished reading file');
    console.log(start);
    console.log(new Date());
    finished = true;
  });
}




function sendSerial(){
  // console.log(myarray[0]);
  // console.log((now-start)/1000);
  HEART_RATE += parseFloat(values[14]);
  THORAX += parseFloat(values[11]);
  counter++;
  if(counter == serial_every){
    var f_THORAX = filterSlide(THORAX,1000);
    // var serial_write = EKG/serial_every+" "+CHIN/serial_every+" "+HEART_RATE/serial_every+" "+f_THORAX/serial_every+"/";
    var serial_write = HEART_RATE/serial_every+" "+f_THORAX/serial_every+" "+position_value+" "+sleep_profile_value+" "+spindle_value+"/";
    HEART_RATE =0;
    THORAX =0;
    // var serial_write = "HELLO";
    // var buff = Buffer.from('test');
    counter = 0;
    if(portOpened){
      sendSerialBuffer(serial_write);
    }
    // port.write(new Buffer('0 0 80 0/','ascii'));
  }
}
function filterSlide(rawValue,slide) {
  // run the filter:
  var result = PREV_THORAX +((rawValue - PREV_THORAX)/slide);
  PREV_THORAX = THORAX;
  // return the result:
  return result;
}
function sendSerialBuffer(data){
  for(var i=0; i<data.length; i++){
    port.write(new Buffer(data[i], 'ascii'), function(err, results) {
    });
  }
}

function convertToAscii(str) {
  var result = '';
  var currentChar = '';
  var i = 0;
  for (; i < str.length; i += 1) {
    currentChar = str[i].charCodeAt(0).toString(2);
    if (currentChar.length < 8) {
      while (8 - currentChar.length) {
        currentChar = '0' + currentChar;
      }
    }
    result += currentChar;
  }
  return result;
}
if(!argv.s) {
  port.on('data', function(data) {
    console.log(data);
  });
}


function sendUDP(){
    msg =  new osc.Message('/s',values[0],values[1],values[2],values[3],values[4],values[5],values[6],values[7],values[8],values[9],values[10],
                                values[11],values[12],values[13],values[14],values[15],values[16],values[17],values[18],values[19],values[20],
                                values[21],values[22],values[23],values[24],values[25],position_value,sleep_profile_value,spindle_value);
    client.send(msg);

}


function slitArray(a){
  var string = a.toString();
  myarray = string.split(',');

  for(var i = 0; i < myarray.length; i++)
  {
    if(!myarray[i]){
      myarray[i] = "x";
    }
    if(parseFloat(myarray[i]) > parseFloat(values[i]) || parseFloat(myarray[i]) < parseFloat(values[i]) ){
      values[i] = parseFloat(myarray[i]);
    }
  }
  sendUDP();
  if(!argv.s){
    sendSerial();
  }

    timeDiff();

}

function timeDiff(){
  if(y == 0) { start = new Date(); y=1;}
  var timeRunning = new Date()- start;
  var offset = (timeRunning-values[0]*1000)/1000;
  if(offset > 0) {
    timeInterval = 1;
  }
  else if(offset<0){
    timeInterval = 3;
  }
  else {
    timeInterval = 2;
  }
  if(values[0] % 60 == 0) {
    console.log(currentSleepTime);
    console.log("real time running: "+msToTime(timeRunning));
    console.log("text time running: "+msToTime(values[0]*1000));
    console.log("offset: "+ offset);
    console.log("  ");
  }
  if(values[0] % 1 == 0) {
    calculateCurrentTime(new Date('2016-04-22 '+msToTime(timeRunning)));
    checkTexts();
  }

}

function msToTime(s) {
  function addZ(n) {
    return (n<10? '0':'') + n;
  }
  var ms = s % 1000;
  s = (s - ms) / 1000;
  var secs = s % 60;
  s = (s - secs) / 60;
  var mins = s % 60;
  var hrs = (s - mins) / 60;

  return addZ(hrs) + ':' + addZ(mins) + ':' + addZ(secs) + '.' + ms;
}

function calculateCurrentTime(timeRunning){
  currentSleepTime.setSeconds(startSleepTime.getSeconds() + timeRunning.getSeconds());
  currentSleepTime.setMinutes(startSleepTime.getMinutes() + timeRunning.getMinutes());
  currentSleepTime.setHours(startSleepTime.getHours() + timeRunning.getHours());

}

function checkTexts(){
  //Check Position

  if(pos_counter <= position.length) {
    var date = new Date('2016-04-22 '+position[pos_counter][0]);
    if(date <= currentSleepTime ) {
      pos_counter++;
      position_value = position[pos_counter-1][2];
    }

  }

  //Check Sleep Profile
  if(sleepProf_counter <= sleepProfile.length) {
    var date = new Date('2016-04-22 '+sleepProfile[sleepProf_counter][0]);
    if(date <= currentSleepTime ) {
      sleepProf_counter++;
      sleep_profile_value = sleepProfile[sleepProf_counter-1][2];
    }

  }

  //Check Spindles
  if(spindle_counter <= spindles.length) {
    var date = new Date('2016-04-22 '+spindles[spindle_counter][0]);
    if(date - currentSleepTime < 0) {
      spindle_counter++;
      spindle_value = spindles[spindle_counter-1][5];
    }
    else{
      spindle_value = "";
    }


  }

  // console.log(currentSleepTime);
  // console.log(position_value);
  // console.log(sleep_profile_value);
  // console.log(spindle_value);

}
